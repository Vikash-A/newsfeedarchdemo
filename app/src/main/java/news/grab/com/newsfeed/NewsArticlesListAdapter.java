package news.grab.com.newsfeed;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import java.util.List;


public class NewsArticlesListAdapter extends RecyclerView.Adapter<NewsViewHolder> {


  private final Picasso picasso;

  private List<NewsArticle> newsArticleList;

  private RecycleViewItemClickListener itemClickListener;


  public NewsArticlesListAdapter(List<NewsArticle> newsArticleList, RecycleViewItemClickListener listener,
      Picasso picasso) {
    super();
    this.picasso = picasso;
    this.newsArticleList = newsArticleList;
    this.itemClickListener = listener;
  }


  @NonNull
  @Override
  public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
    View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.news_item_row, parent, false);
    return new NewsViewHolder(v, itemClickListener);

  }

  @Override
  public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int position) {

    NewsArticle article = newsArticleList.get(position);

    newsViewHolder.txtTitle.setText(article.title);

    newsViewHolder.txtDes.setText(article.description);

    newsViewHolder.txtSource.setText(article.source+"  "+article.publishedAt);

    picasso.load(article.urlToImage).into(newsViewHolder.imageView);

  }

  @Override
  public int getItemCount() {
    return newsArticleList != null ? newsArticleList.size() : 0;
  }

}