package news.grab.com.newsfeed;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {NewsArticle.class}, version = 1, exportSchema = false)

public abstract class NewsDb extends RoomDatabase {

  public abstract NewsArticleDao newsArticleDao();

}
