package news.grab.com.newsfeed;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import java.util.List;

@Dao
public interface NewsArticleDao {

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  long[] insertNewsArticles(List<NewsArticle> articles);

  @Query("SELECT * FROM `NewsArticle`")
  List<NewsArticle> getNewsArticles();

}
