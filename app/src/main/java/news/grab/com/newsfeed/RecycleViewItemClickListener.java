package news.grab.com.newsfeed;

import android.view.View;

public interface RecycleViewItemClickListener {

  void onItemClicked(View v, int position);

}
