package news.grab.com.newsfeed;

import android.app.Application;
import android.content.Context;
import dagger.Binds;
import dagger.Module;
import javax.inject.Singleton;
import news.grab.com.newsfeed.NewsApplication;

/**
 * Provider application-wide dependencies.
 */
@Module
public interface AppModule {
  @Binds
  @Singleton
  Context bindContext(Application application);

  @Binds
  Application bindApplication(NewsApplication app);
}