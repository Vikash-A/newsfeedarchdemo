package news.grab.com.newsfeed;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

  @ContributesAndroidInjector
  abstract MainFragment provideMainFragment();

  @ContributesAndroidInjector
  abstract WebViewFragment provideWebViewFragment();

}


