package news.grab.com.newsfeed;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Provider application-wide dependencies.
 */
@Module
public class ConnectivityModule {

  @Provides
  @Singleton
  LiveData<Boolean> bindContext(Application application) {
    MutableLiveData<Boolean> connectivityLiveData = new MutableLiveData<>();
    ConnectivityManager.NetworkCallback connectivityCallback = new ConnectivityManager.NetworkCallback() {
      @Override
      public void onAvailable(Network network) {
        connectivityLiveData.postValue(true);
      }

      @Override
      public void onLost(Network network) {
        connectivityLiveData.postValue(false);
      }
    };
    final ConnectivityManager connectivityManager = (ConnectivityManager) application.getSystemService(
        Context.CONNECTIVITY_SERVICE);
    final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    boolean isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    connectivityLiveData.postValue(isConnected);
    connectivityManager.registerNetworkCallback(
        new NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build(),
        connectivityCallback);

    return connectivityLiveData;
  }

}