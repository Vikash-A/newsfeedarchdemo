package news.grab.com.newsfeed;

import android.graphics.Outline;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

  private final RecycleViewItemClickListener recycleViewItemClickListener;

  public final TextView txtTitle, txtDes, txtSource;

  public ImageView imageView;


  ViewOutlineProvider outlineProvider = new ViewOutlineProvider() {
    @Override
    public void getOutline(View view, Outline outline) {

      float radius = view.getResources().getDimension(R.dimen.corner_radius);

      outline.setRoundRect(0, 0, view.getWidth(), view.getHeight()+(int)radius , radius);
    }
  };

  public NewsViewHolder(View itemView, RecycleViewItemClickListener listener) {
    super(itemView);
    recycleViewItemClickListener = listener;

    txtTitle = itemView.findViewById(R.id.title);
    txtDes = itemView.findViewById(R.id.des);
    txtSource = itemView.findViewById(R.id.source_time);

    imageView = itemView.findViewById(R.id.image);
    imageView.setClipToOutline(true);

    imageView.setOutlineProvider(outlineProvider);

    itemView.setOnClickListener(this);


  }

  @Override
  public void onClick(View v) {
    if (recycleViewItemClickListener != null) {
      recycleViewItemClickListener.onItemClicked(v, getAdapterPosition());
    }
  }

}