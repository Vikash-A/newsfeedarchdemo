//package news.grab.com.newsfeed;
//
//import android.support.annotation.MainThread;
//import android.support.annotation.NonNull;
//import android.support.annotation.WorkerThread;
//
//import io.reactivex.Flowable;
//import io.reactivex.Observable;
//import io.reactivex.Single;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.schedulers.Schedulers;
//import news.grab.com.newsfeed.Resource;
//
//public abstract class NetworkBoundResource<NetworkResponse, UiResponse> {
//
//    private Single<Resource<UiResponse>> result;
//
//    @MainThread
//    protected NetworkBoundResource() {
//        Observable<Resource<UiResponse>> source;
//        if (shouldFetch()) {
//            source = createCall()
//                .subscribeOn(Schedulers.io())
//                .doOnSuccess(apiResponse -> saveCallResult(processResponse(apiResponse)))
//                .flatMap(apiResponse -> loadFromDb().map(Resource::success))
//                .doOnError(t -> onFetchFailed())
//                .onErrorResumeNext(t -> Single.just(Resource.error(t.getMessage(), null)))
//                .observeOn(AndroidSchedulers.mainThread());
//        } else {
//            source = loadFromDb()
//                .toObservable()
//                .map(Resource::success);
//        }
//
//        result = Observable.concat(
//            loadFromDb()
//                .toObservable()
//                .map(Resource::loading)
//                .take(1),
//            source
//        );
//    }
//
//    public Single<Resource<UiResponse>> getAsObservable() {return result;}
/////**/
//    protected void onFetchFailed() {}
//
//    @WorkerThread
//    protected abstract UiResponse processResponse(NetworkResponse response);
//
//    @WorkerThread
//    protected abstract void saveCallResult(@NonNull UiResponse item);
//
//    @MainThread
//    protected abstract boolean shouldFetch();
//
//    @NonNull
//    @MainThread
//    protected abstract UiResponse loadFromDb();
//
//    @NonNull
//    @MainThread
//    protected abstract Single<NetworkResponse> createCall();
//}