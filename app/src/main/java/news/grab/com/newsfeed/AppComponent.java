package news.grab.com.newsfeed;


import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Component(modules = {
    AppModule.class,
    ViewModelModule.class,
    ActivityModule.class,
    ApiModule.class,
    DbModule.class,
    PicassoModule.class,
    ConnectivityModule.class,
    AndroidSupportInjectionModule.class})
@Singleton
interface AppComponent extends AndroidInjector<NewsApplication> {

  @Component.Builder
  abstract class Builder extends AndroidInjector.Builder<NewsApplication> {

  }
}



