package news.grab.com.newsfeed;

public class NewsHeadlinesResponse {


  public String status;
  public int totalResults;
  public Article[] articles;


  static class Source {
    public String id;
    public String name;
  }

  static class Article {
    public Source source;
    public String author;
    public String title;
    public String description;
    public String url;
    public String urlToImage;
    public String publishedAt;
    public String content;

  }


}
