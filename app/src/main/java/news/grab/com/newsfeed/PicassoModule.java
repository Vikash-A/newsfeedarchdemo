package news.grab.com.newsfeed;

import android.content.Context;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class PicassoModule {

  @Provides
  public Picasso picasso(Context context, OkHttp3Downloader okHttp3Downloader) {
    Picasso picasso = new Picasso.Builder(context)
        .downloader(okHttp3Downloader).build();
    picasso.setIndicatorsEnabled(true);
    picasso.setLoggingEnabled(true);
    return picasso;
  }

  @Provides
  public OkHttp3Downloader okHttp3Downloader(OkHttpClient okHttpClient) {
    return new OkHttp3Downloader(okHttpClient);
  }

}