package news.grab.com.newsfeed;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public class DbModule {


  @Provides
  @Singleton
  NewsDb provideNewsDatabase(@NonNull Application application) {
    return Room.databaseBuilder(application,
        NewsDb.class, "News.db")
        .allowMainThreadQueries().build();
  }


  @Provides
  @Singleton
  NewsArticleDao provideNewsArticleDao(@NonNull NewsDb appDatabase) {
    return appDatabase.newsArticleDao();
  }
}