package news.grab.com.newsfeed;

import java.io.IOException;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class RequestInterceptor implements Interceptor {

  @Override
  public Response intercept(Chain chain) throws IOException {

    Request original = chain.request();
    HttpUrl originalHttpUrl = original.url();

    HttpUrl url = originalHttpUrl.newBuilder()
        .addQueryParameter("apiKey", "b063a9bce942435e8af655377ca846ff")
        .build();

    // Request customization: add request headers
    Request.Builder requestBuilder = original.newBuilder()
        .url(url);

    Request request = requestBuilder.build();
    return chain.proceed(request);

  }
}
