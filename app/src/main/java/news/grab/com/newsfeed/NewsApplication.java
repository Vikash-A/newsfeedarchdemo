package news.grab.com.newsfeed;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class NewsApplication extends DaggerApplication {

  @Override
  public void onCreate() {
    super.onCreate();
    checkConnectivity();
  }

  private void checkConnectivity() {


  }


  @Override
  protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
    return DaggerAppComponent.builder().create(this);
  }
}
