package news.grab.com.newsfeed;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import java.util.List;
import java.util.concurrent.Executor;
import javax.inject.Inject;
import javax.inject.Named;

public class MainViewModel extends ViewModel {

  //@Inject
  private NewsRepository newsRepository;


  private LiveData<List<NewsArticle>> newsArticlesLiveData ;

  @Inject
  public MainViewModel(NewsArticleDao newsArticleDao, NewsService newsService) {
    /* You can see we are initialising the MovieRepository class here */
    newsRepository = new NewsRepository(newsService, newsArticleDao);

    newsArticlesLiveData = newsRepository.getListLiveData();


  }

  public LiveData<List<NewsArticle>> getNewsArticlesLiveData() {
    return newsArticlesLiveData;
  }

  public void loadNewsArticles() {
    newsRepository.loadNewsArticles("IN");
  }


}
