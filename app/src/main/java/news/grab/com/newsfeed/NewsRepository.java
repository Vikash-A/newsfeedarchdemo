package news.grab.com.newsfeed;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import news.grab.com.newsfeed.NewsHeadlinesResponse.Article;

public class NewsRepository {

  private NewsService newsService;

  private NewsArticleDao newsDao;

  private MutableLiveData<List<NewsArticle>> listLiveData = new MutableLiveData<>();

  public MutableLiveData<List<NewsArticle>> getListLiveData() {
    return listLiveData;
  }

  private static final String TAG = "REPO";

  public NewsRepository(NewsService webservice, NewsArticleDao newsDao) {
    this.newsService = webservice;
    this.newsDao = newsDao;
  }

  public void loadNewsArticles(String countryCode) {
    newsService.fetchHeadlines(countryCode).subscribeOn(Schedulers.io()).subscribe(data -> {

      List<NewsArticle> newsArticles = new ArrayList();

      for (Article article : data.articles) {
        NewsArticle newsArticle = NewsArticle.from(article);

        newsArticles.add(newsArticle);

      }

      listLiveData.postValue(newsArticles);
    }, throwable -> {

      // handle error event
    });
  }


}
