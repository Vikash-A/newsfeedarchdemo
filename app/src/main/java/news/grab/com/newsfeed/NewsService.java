package news.grab.com.newsfeed;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {

  @GET("/v2/top-headlines")
  Single<NewsHeadlinesResponse> fetchHeadlines(@Query("country") String country);

}
