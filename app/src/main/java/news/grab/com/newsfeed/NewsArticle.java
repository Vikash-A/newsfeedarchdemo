package news.grab.com.newsfeed;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import news.grab.com.newsfeed.NewsHeadlinesResponse.Article;


@Entity
public class NewsArticle {

  @PrimaryKey(autoGenerate = true)
  public long id;
  public String source;
  public String author;
  public String title;
  public String description;
  public String url;
  public String urlToImage;
  public String publishedAt;
  public String content;


  public static NewsArticle from(Article article) {
    NewsArticle newsArticle = new NewsArticle();

    newsArticle.author = article.author;
    newsArticle.content = article.content;
    newsArticle.description = article.description;
    newsArticle.publishedAt = article.publishedAt;
    newsArticle.title = article.title;
    newsArticle.url = article.url;
    newsArticle.urlToImage = article.urlToImage;
    newsArticle.source = article.source.name;

    return newsArticle;

  }
}
