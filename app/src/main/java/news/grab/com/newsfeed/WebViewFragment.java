package news.grab.com.newsfeed;

import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import dagger.android.support.DaggerFragment;
import javax.inject.Inject;


public class WebViewFragment extends DaggerFragment {

  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  String ERROR_PAGE = "file:///android_asset/error.html";

  @Inject
  LiveData<Boolean> connectivityLiveData;


  private String mParam1;
  private String mParam2;

  private WebView mWebView;

  private int loadingState;


  public WebViewFragment() {
    // Required empty public constructor
  }


  public static WebViewFragment newInstance(String param1, String param2) {
    WebViewFragment fragment = new WebViewFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_web_view, container, false);

    mWebView = view.findViewById(R.id.webview);

    mWebView.setWebChromeClient(new WebChromeClient());

    mWebView.setWebViewClient(new WebViewClient() {
      public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        mWebView.loadUrl(ERROR_PAGE);
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (ERROR_PAGE.equals(url)) {
          loadingState = -1;
        }

      }
    });

    mWebView.getSettings().setJavaScriptEnabled(true);

    mWebView.setOnKeyListener((v, keyCode, event) -> {
      if (keyCode == KeyEvent.KEYCODE_BACK
          && event.getAction() == MotionEvent.ACTION_UP
          && mWebView.canGoBack()) {
        mWebView.goBack();
        return true;
      }
      return false;
    });

    mWebView.loadUrl(mParam1);

    connectivityLiveData.observe(this, isConnected -> {
      if (isConnected != null && isConnected) {
        if (loadingState == -1 && mWebView.canGoBack()) {
          mWebView.goBack();
        } else if (loadingState == -1) {
          mWebView.loadUrl(mParam1);
        }
      }
    });

    return view;
  }


}
