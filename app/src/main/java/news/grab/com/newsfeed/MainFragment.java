package news.grab.com.newsfeed;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.squareup.picasso.Picasso;
import dagger.android.support.DaggerFragment;
import java.util.List;
import javax.inject.Inject;

public class MainFragment extends DaggerFragment {

  private MainViewModel mViewModel;

  @Inject
  ViewModelFactory viewModelFactory;

  @Inject
  Picasso picasso;

  private RecyclerView recyclerView;


  @Inject
  LiveData<Boolean> connectivityLiveData;


  public static MainFragment newInstance() {
    return new MainFragment();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    View root = inflater.inflate(R.layout.main_fragment, container, false);

    recyclerView = root.findViewById(R.id.recyclerView);

    mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

    mViewModel.getNewsArticlesLiveData().observe(this, this::prepareUi);

    connectivityLiveData.observe(this, isConnected -> {

      if (isConnected != null && isConnected) {

        mViewModel.loadNewsArticles();

      }

    });

    return root;
  }


  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
  }

  private void prepareUi(final List<NewsArticle> newsArticles) {
    if (newsArticles != null) {
      recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
      recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
      recyclerView.setAdapter(
          new NewsArticlesListAdapter(newsArticles, (v, position) -> {

            NewsArticle article = newsArticles.get(position);

            if (isAdded()) {
              getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("main")
                  .replace(R.id.container, WebViewFragment.newInstance(article.url, ""))
                  .commit();

            }


          }
              , picasso));
    }
  }
}
