package news.grab.com.newsfeed;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import dagger.android.support.DaggerAppCompatActivity;
import javax.inject.Inject;

public class MainActivity extends DaggerAppCompatActivity {



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);
    if (savedInstanceState == null) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.container, MainFragment.newInstance())
          .commitNow();
    }
  }
}
